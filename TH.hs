
module TH where

import qualified Data.Aeson.TH as Aeson

import Text.Casing

deriveJSON = Aeson.deriveJSON Aeson.defaultOptions
      { Aeson.fieldLabelModifier = kebab . dropWhile (`elem` "qwertyuiopasdfghjklzxcvbnm")
      }
