
module MathEntities where

import Foreign.C.Types (CInt)
import qualified SDL

import TH

data Point = Point { pX, pY :: Int } deriving (Eq, Show)
data Size  = Size  { sW, sH :: Int } deriving (Eq, Show)
data Grid  = Grid  { gCellWidth, gCellHeight :: Int } deriving (Eq, Show)
data Rect  = Rect  { rPoint :: Point, rSize :: Size } deriving (Eq, Show)

data PalettePosition = PalettePosition
    { ppPoint :: Point
    , ppSize  :: Maybe Size
    }
    deriving (Eq, Show)

conv :: (Integral a, Num b) => a -> b
conv = fromIntegral

class CanShift c where
    shift :: Point -> c -> c

instance CanShift Point where
    shift (Point dx dy) (Point x y) = Point (x + dx) (y + dy)

instance CanShift Rect where
    shift d (Rect pt sz) = Rect (shift d pt) sz

class HasSDLRepresentation h where
    type SDL h :: *

    toSdl   :: h -> SDL h
    fromSdl :: SDL h -> h

instance HasSDLRepresentation Point where
    type SDL Point = SDL.Point SDL.V2 CInt

    toSdl (Point x y) = SDL.P (SDL.V2 (conv x) (conv y))
    fromSdl (SDL.P (SDL.V2 (conv -> x) (conv -> y))) = Point x y

instance HasSDLRepresentation Size where
    type SDL Size = SDL.V2 CInt

    toSdl (Size w h) = SDL.V2 (conv w) (conv h)
    fromSdl (SDL.V2 (conv -> w) (conv -> h)) = Size w h

instance HasSDLRepresentation Rect where
    type SDL Rect = SDL.Rectangle CInt

    toSdl (Rect pt sz) = SDL.Rectangle (toSdl pt) (toSdl sz)
    fromSdl (SDL.Rectangle (fromSdl -> pt) (fromSdl -> sz)) = Rect pt sz

deriveJSON ''Point
deriveJSON ''Size
deriveJSON ''Rect
deriveJSON ''PalettePosition
deriveJSON ''Grid
