
import Control.Monad
import Control.Monad.IO.Class

import Data.Aeson
import Data.Char
import qualified Data.Map as Map

import GHC.Generics

import SDL hiding (Point)
import SDL.Video.Renderer
import SDL.Image

import System.FilePath

import MathEntities
import TH

data Tile = Tile Rect Surface
    deriving (Generic)

data TileSet = TileSet
    { tsTiles :: Map.Map String Tile
    }
    deriving (Generic)

data TileSetCfg = TileSetCfg
    { tscTiles  :: Map.Map String PalettePosition
    , tscSize   :: Grid
    , tscSource :: FilePath
    }
    deriving (Eq, Show)

deriveJSON ''TileSetCfg

createTileset :: MonadIO m => TileSetCfg -> m TileSet
createTileset (TileSetCfg tiles size source) = do
    surf <- load source
    return $ TileSet $ fmap (\pt -> makeTile pt size surf) tiles
  where
    makeTile (PalettePosition point override) (Grid cw ch) =
        let
            Size w h = maybe (Size 1 1) id override
        in
            Tile (Rect point (Size (cw * w) (ch * h)))

data Source = Source
    { sInput :: Surface
    , sRect  :: Rect
    }

data Destination = Destination
    { dOutput :: Surface
    , dRect   :: Rect
    }

blit :: MonadIO m => Source -> Point -> Destination -> m ()
blit (Source input irect) offset (Destination output orect) =
    void $ surfaceBlit input (Just $ toSdl irect) output (Just $ toSdl $ rPoint (shift offset orect))

main = do
    Just tsc <- decodeFileStrict "terrain.json"
    print (tsc :: TileSetCfg)
